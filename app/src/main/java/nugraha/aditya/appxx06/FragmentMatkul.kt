package nugraha.aditya.appxx06

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_matkul.*
import kotlinx.android.synthetic.main.frag_data_matkul.view.*

class FragmentMatkul : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner2.setSelection(0,true)

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter.getItem(position) as Cursor
        namaProdi = c.getString(c.getColumnIndex("_id"))

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteMatkul ->{

            }
            R.id.btnInsertMatkul ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("APAKAH DATA YANG DIMASUKKAN SUDAH BENAR ?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdateMatkul ->{

            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v :View
    var kodeMatkul : String=""
    var namaProdi : String=""
    var namaMatkul : String=""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_matkul,container, false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteMatkul.setOnClickListener(this)
        v.btnInsertMatkul.setOnClickListener(this)
        v.btnUpdateMatkul.setOnClickListener(this)
        v.spinner2.onItemSelectedListener =  this
        return v
    }

    override fun onStart(){
        super.onStart()
        showDataMatkul("")
        showDataProdi( )
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        namaMatkul = c.getString(c.getColumnIndex("_id"))
        v.edNamaMatkul.setText(c.getString(c.getColumnIndex("nama")))
    }

    fun showDataMatkul(kodeMatkul : String){
        var sql=""
        if(!kodeMatkul.trim().equals(" ")){
            sql = "select k.kode as _id, k.nama_matkul, p.nama_prodi from matkul k, prodi p " +
                    "where k.id_prodi=p.id_prodi and k.nama_matkul like '%$kodeMatkul%'"
        }else{
            sql = "select k.kode as _id, k.nama_matkul. p.nama_prodi from matkul k, prodi p " +
                    "where k.id_prodi=p.id_prodi order by k.kode asc"
        }

        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_matkul,c,
            arrayOf("_id","nama_matkul","nama_prodi"), intArrayOf(R.id.txKodeMatkul, R.id.txNamaMatkul, R.id.txProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMatkul.adapter = lsAdapter
    }

    fun showDataProdi(){
        val  c : Cursor = db.rawQuery("select nama_prodi as _id from prodi order by nama_prodi asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner2.adapter = spAdapter
        v.spinner2.setSelection(0)
    }

    fun insertDataMatkul(kodeMatkul: String, namaMatkul: String, id_prodi : Int){
        var sql = "insert into matkul(kode, nama_matkul, id_prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(kodeMatkul, namaMatkul, id_prodi))
        showDataMatkul("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi='$namaProdi'"
        val c : Cursor = db.rawQuery(sql,null)
        if (c.count>0){
            c.moveToFirst()
            insertDataMatkul(v.edKodeMatkul.text.toString(), v.edNamaMatkul.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edKodeMatkul.setText("")
            v.edNamaMatkul.setText("")
        }
    }

}